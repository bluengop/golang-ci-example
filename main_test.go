package main

import (
  "fmt"
  "testing"
  "regexp"
)

func TestHelloName (t *testing.T) {
  name := "Daniela"
  want := regexp.MustCompile("Hello "+name+"!")
  fmt.Println(want)
  msg := HelloName(name)
  if !want.MatchString(msg) {
    t.Fatalf("The message is not what we expect: %v", msg)
  }
}

