package main

import (
  "fmt"
  "os"
)

func main() {
  var name string
  if len(os.Args) == 2 {
    name = os.Args[1]
  } else {
    name = os.Getenv("USER")
  }

  fmt.Println(HelloName(name))
}

func HelloName (n string) string {
  return fmt.Sprintf("Hello %s!", n)
}

