.PHONY: run
run:
	@go run ./main.go $(NAME)

.PHONY: test
test:
	@go test *.go
